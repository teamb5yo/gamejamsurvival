﻿using UnityEngine;
using System.Collections;

public class SwitchingActivePlayer : MonoBehaviour {

	public GameObject player;
	Vector3 temp;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnMouseDown(){
		temp = transform.position;
		transform.position = player.transform.position;
		player.transform.position = temp;
	}

}
