﻿using UnityEngine;
using System.Collections;

public class _AIMovement : MonoBehaviour
{

    public float rotationSpeed;
    public float movementSpeed;
    public float rotationTime;

    public float worldSize;

    void Start()
    {
        Invoke("ChangeRotation", rotationTime);
    }

    void ChangeRotation()
    {
        if (Random.value > 0.5f)
        {
            rotationSpeed = -rotationSpeed;
        }
        Invoke("ChangeRotation", rotationTime);
    }

    void OnCollisionEnter(Collision collision)
    {

        transform.Rotate(new Vector3(0, 0, 90));
       
        transform.position += transform.up * movementSpeed * Time.deltaTime *-1;
    }

    void Update()
    {
        transform.Rotate(new Vector3(0, 0, rotationSpeed * Time.deltaTime));
        transform.position += transform.up * movementSpeed * Time.deltaTime;

          
    }
}

