﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float speed;
	public GameObject punch;
	public AudioClip punchSound;
	AudioSource audio;
	
	private int frames;

	void Start (){
		audio = GetComponent<AudioSource> ();
	}

	// Update is called once per frame
	void Update () {

		// Punching and frames timer
		frames++;
		if (Input.GetMouseButton(1)&& frames >=60) {

			frames = 0;
			punch.SetActive(true);
			audio.PlayOneShot(punchSound, 1F);

		} 

		if(frames >= 30){
			punch.SetActive(false);
		}
		
	
		// movement

		if (Input.GetKey (KeyCode.W)) {
			GetComponent<Rigidbody>().AddForce(Vector2.up * speed);
		}

		if (Input.GetKey (KeyCode.A)) {
			GetComponent<Rigidbody>().AddForce(-Vector2.right * speed);
		}

		if (Input.GetKey (KeyCode.S)) {
			GetComponent<Rigidbody>().AddForce(-Vector2.up * speed);
		}

		if (Input.GetKey (KeyCode.D)) {
			GetComponent<Rigidbody>().AddForce(Vector2.right * speed);
		}

		// rotation sprite to moving direction
		Vector2 moveDirection = GetComponent<Rigidbody>().velocity;
		if (moveDirection != Vector2.zero) {
			float angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
		}

	}
}
